from dataclasses import dataclass
from pathlib import Path
from typing import Optional, Sequence
import torch
import os
@dataclass 
class Args:
    
    #-- random seed
    seed : int = 41 
    
    #-- logger
    logger : str = 'csv' # "csv" "wandb"
    
    #-- wandb
    project_name : str = 'IPEO'
    run_name : str = 'Debug'
    tag : Sequence[str] = ('V0')
    
    #-- data loader
    data_location : str ='./data/ipeo-data'
    pin_memory : bool = True 
    num_workers : int = os.cpu_count()//2
    num_devices : int = 1 # number of GPUs. only trains with one GPU.
    
    #-- smp Model parameters
    model_name : str = 'PSPNet'
    available_models : Sequence[str] = ('unet','FPN','FCN','Linknet','MAnet','PSPNet')
    encoder_name: str = 'efficientnet-b3' # See https://smp.readthedocs.io/en/latest/encoders.html
    output_path: Optional[Path] = None
    decoder_use_batchnorm : bool = True
    encoder_weights : str = "imagenet"
    encoder_depth: int = 5 # do not change
    in_channels : int = 3
    num_classes : int = 8
    input_size : int = 416 # it's a square DO NOT CHANGE!!
    activation : Optional[str] = None # for final convolution layer “sigmoid”, “softmax”, “logsoftmax”, “tanh”, “identity”, callable and None. 
    decoder_attention_type = None
    pretraining_weights : str = 'None'
    save_weights_only : bool = True
    
    #-- Training params
    lr : float = 1e-3
    optimizer : str = "Adam" # or "SGD" "Adam"
    batch_size : int = 32
    max_epochs : int = 20
    criterion : str = "ce" # "dice", "ce", "dice+ce","focal"
    focal_loss_gamma : float = 1.0
    device : str = "gpu" if torch.cuda.is_available() else "cpu"
    max_steps : int = -1 # if set to another value then it will be the earliest between max_epochs and max_steps
    max_time = "00:12:00:00" # stop after 12 hours
    weight_decay : float = 1e-6 
    precision : int = 32
    gradient_clipping_algo : str = 'norm'# 'norm' or 'value'
    gradient_clipping_val : float = 10.

    #-- Lr scheduling
    metric_to_monitor_lr_scheduler : str = "valid_loss"
    lr_scheduler : str = "CosineAnnealingWarmRestarts" # Other options are "OneCycleLR" "CosineAnnealingWarmRestarts", "ReduceLROnPlateau"
    lr_scheduler_patience : int = 3
    lr_scheduler_factor : float = 0.1 # <1 !
    min_lr : float = 1e-6
    lr_scheduler_mode : str = 'min' # goal is to minimize the metric to monitor
    
    #-- Early stopping
    metric_to_monitor_early_stop: str = "valid_dice_score"
    early_stopping_patience:int = 10
    early_stopping : bool = False
    early_stop_mode : str = "max" # goal is to maximize the metric to monitor
    
    #-- logging
    log_pred_every_nstep : int = 1000
    log_pred_every_nepoch : int = 5
    disable_media_logging : bool = False # works only when using logger='wandb'!

    

