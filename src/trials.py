# -*- coding: utf-8 -*-
"""
Created on Sat Dec 24 00:09:11 2022

@author: fadel
"""

#-- plotting prediction image
from models import Architecture
import torchvision.transforms as T 
from PIL import Image
from args import Args
from datargs import parse
import torch
import numpy as np
import matplotlib.pyplot as plt

weights_paths ={
    'pspnet/dice+ce':"./weigths/18wdrp6p/checkpoints/epoch=15-step=1840.ckpt",
    }

#-- Get model
torch.set_grad_enabled(mode=False)
args = parse(Args)
args.model_name = 'PSPNet'
args.logger = 'csv'
model = Architecture(args)
model.eval()

#-- Load image
to_Tensor = T.functional.to_tensor
Resize = T.Resize((416, 416),interpolation=T.InterpolationMode.NEAREST)

rgb_path = './data/25556_11105_rgb.tif'
truth_path = './data/25556_11105_label.tif'
img_rgb = to_Tensor(np.array(Image.open(rgb_path)).astype('int16'))
img_truth = to_Tensor(np.array(Image.open(truth_path)).astype('int16'))
img_rgb = img_rgb/255.0 # scale

img,gd_mask = Resize(img_rgb.float()), img_truth.squeeze().long()
img = img[None,...]

for name,path in weights_paths.items():
    print('Doing : ',name, path)
    model = model.load_from_checkpoint(path,args=args)
    logits_mask = model(img)
    prob_mask = torch.softmax(logits_mask,dim=1)
    pred_mask = torch.argmax(prob_mask, dim=1)

fig,axs = plt.subplots(1,3,figsize=(15,9))
plt.imshow(img.cpu().numpy().squeeze())
plt.imshow(img_truth.cpu().numpy().squeeze())
plt.imshow(pred_mask.cpu().numpy().squeeze())
plt.savefig('./data/predictions.png')