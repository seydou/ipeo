import segmentation_models_pytorch as smp
import pytorch_lightning as pl
import torch
import torchvision
import torchmetrics 
import wandb
from torch.utils.data import Dataset
from PIL import Image
import numpy as np
import pandas as pd
import torchvision.transforms as T 
# from skimage import io
import os
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from pytorch_lightning.callbacks import LearningRateMonitor
from pytorch_lightning.loggers import WandbLogger,CSVLogger
from torch.utils.data import DataLoader#,ConcatDataset


def get_transforms(mode='train'):
    """This function return the transformation used when trainin the model.
    

    Parameters
    ----------
    mode : str, optional
        It should be 'train','val' or 'test'. The default is 'train'.

    Returns
    -------
    transforms : TYPE
        DESCRIPTION.

    """
    assert mode in ['train','test','val']
    
    if mode == 'train':
        transforms = T.Compose([
                T.RandomHorizontalFlip(),
                T.RandomVerticalFlip(),
                T.RandomGrayscale(0.1),
                T.GaussianBlur(kernel_size=5),
        ])
        
    else:
        transforms = None
        
        
    return transforms



def log_batch_to_wandb(img,ground_truth_mask,prediction_mask:torch.Tensor,stage:str,epoch:int,step:int,args):
    """This function is ussed to log images to wandb if it is chosen as the logger.
    

    Parameters
    ----------
    img : torch.Tensor
        4D tensor of shape [B,C,H,W].
    ground_truth_mask : torch.Tensor
        4D tensor of shape [B,C,H,W].
    prediction_mask : torch.Tensor
        4D tensor of shape [B,C,H,W].
    stage : str
        It should be 'train', 'valid' or 'test'.
    epoch : int
        epoch number.
    step : int
        gradient step number.
    args : dataclass
        configuration.

    Returns
    -------
    None.

    """
    # img, ground_truth_mask = batch
    class_labels = {
        7:'rocks',
        6:'scree',
        5:'sparse rocks',
        3:'water',
        4:'glacier and permanent snow',
        1:'forest',
        2:'sparse forest',
        0:'grassland and others'
    }
    
    # Unnormalized dataset
    # std_inv = 1 / (STD + 1e-7)
    # unnormalize = T.Normalize(-MEAN * std_inv, std_inv)
    # img = unnormalize(img)

    conf = dict(nrow=2,pad_value=0,scale_each=False,normalize=True,padding=0)
    
    assert ground_truth_mask.ndim == 4, 'requirement for make_grid'
    assert prediction_mask.ndim == 4, 'requirement for make_grid'
    
    resizer = T.Resize((400, 400),interpolation=T.InterpolationMode.NEAREST)
    
    img = resizer(img).cpu()
    ground_truth_mask = resizer(ground_truth_mask).cpu()
    prediction_mask = resizer(prediction_mask).detach().cpu()
    
    #-- plot
    img_grid = torchvision.utils.make_grid(img,**conf).numpy().transpose((1,2,0)) #-- Disabling logging of images
    gmask_grid = torchvision.utils.make_grid(ground_truth_mask/7,value_range=(0,1),**conf).numpy()[0]*7
    predmask_grid = torchvision.utils.make_grid(prediction_mask/7,value_range=(0,1),**conf).numpy()[0]*7
    
    #-- Log
    mask_info = {
        "predictions" : {
            "mask_data" : predmask_grid.astype('int'),
            "class_labels" : class_labels
            },
        "ground_truth" : {
            "mask_data" : gmask_grid.astype('int'),
            "class_labels" : class_labels
            }
        }
    
    wandb.log({f"Batch_data_{stage}" : wandb.Image(img_grid,
                                            caption = f"{stage}_epoch_{epoch}_step_{step}", 
                                            masks=mask_info)})
      
    
    return None



class AlpineImages(Dataset):
    
    #-- mapping between labels and classes
    label_classes = {
        'rocks' : 7,
        'scree' : 6,
        'sparse rocks' : 5,
        'water' : 3,
        'glacier and permanent snow' : 4,
        'forest' : 1,
        'sparse forest' : 2,
        'grassland and others' : 0
    }

    def __init__(self, transforms = None, split = 'train', data_path='./ipeo_data'):
        self.transforms = transforms

        #-- grab the list of files that we need for splitting the data
        split_files = pd.read_csv(data_path+f'/splits/{split}.csv', header=None)
        split_files = split_files.loc[split_files[0] != '25595_11025'] #We know this file has corrupt data
        #-- create rows that contain the values that we will use to get different images later on
        split_files['rgb_path'] = split_files.loc[:,0].apply(lambda x: data_path+'/rgb/'+x+'_rgb.tif')
        split_files['truth_path'] = split_files.loc[:,0].apply(lambda x: data_path+'/alpine_label/'+x+'_label.tif')
        #store the the columns in variables as a list
        self.data = split_files.rgb_path.dropna().tolist()
        self.truth = split_files.truth_path.dropna().tolist()
       
        assert len(self.data) == len(self.truth),'The truth list and the data list are not the same length'

        self.to_Tensor = T.functional.to_tensor
        self.Resize = T.Resize((416, 416),interpolation=T.InterpolationMode.NEAREST)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, x):
        rgb_path = self.data[x]
        truth_path = self.truth[x]

        img_rgb = self.to_Tensor(np.array(Image.open(rgb_path)).astype('int16'))
        img_truth = self.to_Tensor(np.array(Image.open(truth_path)).astype('int16'))
        #-- set a seed so that the transforms work the same for both truths and images
        seed = np.random.randint(214567)
        np.random.seed(seed=seed)
        
        img_rgb = img_rgb/255.0 # Min-Max rescaling

        if self.transforms is not None:
            img_rgb = self.transforms(img_rgb.float())
            img_truth = self.transforms(img_truth.float())
            
        return self.Resize(img_rgb.float()), img_truth.squeeze().long()



def create_model(args):
    """This function load the wanted model.
    

    Parameters
    ----------
    args : dataclass
        configurations.

    Raises
    ------
    NotImplementedError
        It happens when the user requires a model that doesn't exist.

    Returns
    -------
    model : torch.nn.Module
        

    """
    #-- create model  
    if args.model_name == 'unet':
        model = smp.Unet(encoder_name=args.encoder_name,
                             encoder_depth=args.encoder_depth, 
                             decoder_use_batchnorm=args.decoder_use_batchnorm,
                             decoder_channels=(256,128,64,32,16),
                             decoder_attention_type=args.decoder_attention_type,
                             encoder_weights=args.encoder_weights,
                             in_channels=args.in_channels,
                             classes=args.num_classes,
                             activation=args.activation,aux_params=None)
        
    elif args.model_name == 'PSPNet':
        model = smp.PSPNet(encoder_name=args.encoder_name,
                           encoder_weights=args.encoder_weights,
                           encoder_depth=args.encoder_depth,
                           psp_out_channels=512,
                           psp_use_batchnorm=args.decoder_use_batchnorm,
                           psp_dropout=0.5,
                           in_channels=args.in_channels,
                           classes=args.num_classes,
                           activation=args.activation,
                           upsampling=32, aux_params=None)
            
    elif args.model_name == 'FPN':
            
        model = smp.FPN(encoder_name=args.encoder_name,
                        encoder_depth=args.encoder_depth, 
                        encoder_weights='imagenet', 
                        decoder_pyramid_channels=256, 
                        decoder_segmentation_channels=128, 
                        decoder_merge_policy='add', 
                        decoder_dropout=0.5, 
                        in_channels=args.in_channels,
                        classes=args.num_classes,
                        activation=args.activation,
                        upsampling=4, 
                        aux_params=None)
        
    elif args.model_name == 'FCN':
         model =  torchvision.models.segmentation.fcn_resnet50(num_classes=args.num_classes)
            
        
    elif args.model_name == 'Linknet':
        model = smp.Linknet(encoder_name=args.encoder_name, 
                                   encoder_depth=args.encoder_depth, 
                                   encoder_weights=args.encoder_weights, 
                                   decoder_use_batchnorm=args.decoder_use_batchnorm, 
                                   in_channels=args.in_channels, 
                                   classes=args.num_classes, 
                                   activation=args.activation, 
                                   aux_params=None)            
            
    elif args.model_name == 'MAnet':
            
        model = smp.MAnet(encoder_name=args.encoder_name,
                             encoder_depth=args.encoder_depth, 
                             decoder_use_batchnorm=args.decoder_use_batchnorm,
                             decoder_channels=(256,128,64,32,16),
                             encoder_weights=args.encoder_weights,
                             in_channels=args.in_channels,
                             classes=args.num_classes,
                             activation=args.activation,aux_params=None)
        
    else:
        print(args.model_name, " is not implemented!")
        raise NotImplementedError
    
    return model



#-- Pytorch lightning model
class Architecture(pl.LightningModule):

    def __init__(self,args):
        super().__init__()
        
        self.args = args # configuration of the run
        
        #-- dice coefficient
        self.dice_coeff_train = torchmetrics.Dice(zero_division=1,num_classes=self.args.num_classes,
                                                  multiclass=True,average='macro',mdmc_average='samplewise') 
        self.dice_coeff_valid = torchmetrics.Dice(zero_division=1,num_classes=self.args.num_classes,
                                                  multiclass=True,average='macro',mdmc_average='samplewise')
        
        self.model = create_model(self.args) # get model
        
        #-- get losses
        self.loss_dice = smp.losses.DiceLoss(mode='multiclass', from_logits=True)
        self.loss_focal = smp.losses.FocalLoss(mode='multiclass',gamma=self.args.focal_loss_gamma)
        self.loss_ce = torch.nn.CrossEntropyLoss(reduction='mean')
        
        #-- data transformation
        MEAN = [0.432, 0.479, 0.421] # from dataset
        STD = [0.237, 0.223, 0.193] # from dataset
        self.normalize =  T.Normalize(MEAN,STD)

        if self.args.criterion not in ["dice", "ce", "dice+ce","focal"]:
            raise NotImplementedError

        
    def forward(self, image):
        """forward pass
        

        Parameters
        ----------
        image : torch.Tensor
            A 4D tensor of shape [B,C,H,W] .

        Returns
        -------
        logits_mask : torch.Tensor
            A 4D tensor of shape [B,C,H,W] that contains the logits of the network.

        """
        image = self.normalize(image)
        mask = self.model(image) if self.args.model_name != 'FCN' else self.model(image)['out']
        
        logits_mask = torch.nn.functional.interpolate(mask,size=200,mode='bilinear') # interpolate to get the same shape as the mask

        return logits_mask

    def shared_step(self, batch, stage, batch_idx):
        """This function characterises a specific "step" for a given batch. It helps to comput and log all metrics for training stagr, validation or test stage.
        

        Parameters
        ----------
        batch : tuple
            a tuple of two tensors meaning [image,mask].
        stage : str
            it is the stage. stage can be 'train', 'valid' or 'test'. It shapes how the function runs.
        batch_idx : int
            It is the index of the batch in the dataloader.

        Returns
        -------
        loss
            torch.Tensor.

        """
        image, mask = batch

        #-- checks
        assert image.ndim == 4
        b,c,h, w = image.shape
        assert h % 32 == 0 and w % 32 == 0
        assert mask.ndim == 3
        assert mask.max() < self.args.num_classes and mask.min() >= 0, f"mask.max = {mask.max()}; mask.min = {mask.min()}; num_classes = {self.args.num_classes}"
        
        
        #-- forward pass
        logits_mask = self.forward(image)
        
        #-- compute loss
        if self.args.criterion == "dice":
            loss = self.loss_dice(logits_mask, mask) 
                 
        if self.args.criterion == "focal":
            loss = self.loss_focal(logits_mask, mask)
            
        if self.args.criterion == "ce":
            loss = self.loss_ce(logits_mask,mask)
                        
        if self.args.criterion == "dice+ce":
            loss = self.loss_ce(logits_mask,mask) + self.loss_dice(logits_mask, mask)
        
        
        #-- Prediction
        prob_mask = torch.softmax(logits_mask,dim=1)
        pred_mask = torch.argmax(prob_mask, dim=1)
        
        #-- Logging metrics
        self.log(f"{stage}_loss",loss.item(),prog_bar=True,on_step=False,on_epoch=True,logger=True,sync_dist=self.args.num_devices>1)
        
        if stage == 'train':
            self.dice_coeff_train.update(pred_mask.int(),mask.int())
           
        else:
            self.dice_coeff_valid.update(pred_mask.int(),mask.int())
            self.log(f"{stage}_dice_score", self.dice_coeff_valid, on_epoch=True,on_step=False,logger=True,prog_bar=False,sync_dist=self.args.num_devices>1)
           
       #-- media logging condition
        log_batch =  (batch_idx == 0) and (self.current_epoch % self.args.log_pred_every_nepoch == 0) and self.args.disable_media_logging==False

        #-- Logging visualization
        if (log_batch or (stage=='test' and batch_idx == 0)) and (self.args.logger == 'wandb') :
            
            class_names=  ['grassland and others',
                           'forest',
                           'sparse forest',
                           'water',
                           'glacier and permanent snow',
                           'sparse rocks',
                           'scree',
                           'rocks']
            
            #-- Log confusion matrix
            cm = wandb.plot.confusion_matrix(probs=None,
                        y_true=mask.int().cpu().numpy().flatten(), preds=pred_mask.int().cpu().numpy().flatten(),
                        class_names=class_names)
            wandb.log({f"{stage}_confMat" : cm})

            log_batch_to_wandb(image[:8].cpu(), mask[:8,None,:,:].cpu(),pred_mask[:8,None,:,:].cpu(), stage,
                                self.current_epoch,self.global_step,
                                args=self.args)
        
        if stage == 'test':
            return {'gt':mask.int().cpu().numpy().flatten(),
                    'pred':pred_mask.int().cpu().numpy().flatten()}
            
        return loss

                       
    def training_step(self, batch, batch_idx):
        return self.shared_step(batch, "train",batch_idx)            

    def validation_step(self, batch, batch_idx):
        return self.shared_step(batch, "valid",batch_idx)

    def test_step(self, batch, batch_idx):
        return self.shared_step(batch, "test",batch_idx)  
    
    def test_epoch_end(self, outputs):
        """This function is run at the end of a test epoch. It computes and logs the confusion matrix if the logger used is wandb.
        

        Parameters
        ----------
        outputs : list
            It has the predictions (and groundtruths) computed at every step.

        Returns
        -------
        None.

        """
        gt_masks = np.hstack([out['gt'] for out in outputs])
        pred_mask = np.hstack([out['pred'] for out in outputs])
        
        if self.args.logger == 'wandb':
            #-- Log confusion matrix
            class_names =  ['grassland and others',
                               'forest',
                               'sparse forest',
                               'water',
                               'glacier and permanent snow',
                               'sparse rocks',
                               'scree',
                               'rocks']
            cm = wandb.plot.confusion_matrix(probs=None,
                            y_true=gt_masks.flatten(), preds=pred_mask.flatten(),
                            class_names=class_names)
            wandb.log({"test_confMat" : cm})

        
    def configure_optimizers(self):
        """Here we are setting up the configuration of the optimizers and leraning schedulers we desire to use.
        

        Returns
        -------
        out : dict
            a dictionary with information on the optimizer and learning rate scheduler.

        """
        opt = torch.optim.Adam(self.parameters(), lr=self.args.lr,weight_decay=self.args.weight_decay)
        interval = 'epoch'

        if self.args.optimizer == "SGD":
            opt = torch.optim.SGD(self.parameters(), lr=self.args.lr,weight_decay=self.args.weight_decay,momentum=0.9,nesterov=True)
        
        #--- Default
        sch = torch.optim.lr_scheduler.ReduceLROnPlateau(opt,mode=self.args.lr_scheduler_mode,
                                                             factor=self.args.lr_scheduler_factor,
                                                             patience=self.args.lr_scheduler_patience,
                                                             min_lr=self.args.min_lr)
        
        if self.args.lr_scheduler == "CosineAnnealingWarmRestarts":
            sch = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(opt,
                                                                       T_0=self.args.max_epochs,
                                                                       T_mult=1,
                                                                       eta_min=self.args.min_lr)
        elif self.args.lr_scheduler == "OneCycleLR":
            interval = 'step'
            sch = torch.optim.lr_scheduler.OneCycleLR(opt, 
                                                      max_lr=1e-2,
                                                      epochs=self.args.max_epochs,
                                                      steps_per_epoch=self.trainer.estimated_stepping_batches, # or self.args.len_dataloader
                                                      anneal_strategy='cos')
            
        
        out = {
            "optimizer": opt,
            "lr_scheduler": {
                "scheduler": sch,
                "monitor": self.args.metric_to_monitor_lr_scheduler,
                "frequency": 1,
                "interval":interval
                },
            }
        
        return out


#-- training function
def train(args):
    """This function train the model. Read runner.py tu understand how to use it.
    

    Parameters
    ----------
    args : dataclass
        Configuration of the model.

    Returns
    -------
    None.

    """
    #-- Create loaders
    train_data = AlpineImages(transforms = get_transforms(mode='train'),
                              split = 'train', 
                              data_path=args.data_location)

    train_loader = DataLoader(train_data,
                              batch_size=args.batch_size,
                              num_workers=args.num_workers,
                              pin_memory=args.pin_memory,
                              shuffle=False)
    
    
    val_data = AlpineImages(transforms = get_transforms(mode='val'),
                              split = 'val', 
                              data_path=args.data_location)
    
    val_loader = DataLoader(val_data,
                            batch_size=args.batch_size,
                            num_workers=args.num_workers,
                            pin_memory=args.pin_memory,
                            shuffle=False)
    
    img,target = next(iter(val_loader))    
    print('\nShape of input & target : ', img.shape,target.shape,'\n')
    print('Size of train : ',len(train_data))
    print('Size of val : ',len(val_data),'\n')

    
    #-- Create model
    model = Architecture(args)
       
    #-- Create trainer
    logger = CSVLogger("csv-logs", name="my_exp")
    if args.logger == 'wandb':
        logger = WandbLogger(project=args.project_name,log_model=args.save_weights_only)
        
    print("\n",args,"\n\n")

    #-- Lr monitor
    lr_monitor = LearningRateMonitor(logging_interval='epoch')
    checkpointer = pl.callbacks.ModelCheckpoint(monitor=args.metric_to_monitor_early_stop,mode='max',auto_insert_metric_name=True,
                                                save_on_train_epoch_end=False,save_weights_only=args.save_weights_only)
    callbacks = [lr_monitor,checkpointer]

    #-- Checking for early stopping
    if args.early_stopping:
        callbacks.append(EarlyStopping(monitor=args.metric_to_monitor_early_stop,
                   mode=args.early_stop_mode,check_on_train_epoch_end=False,
                   patience=args.early_stopping_patience,
                   verbose=False,
                   min_delta=5e-4,
                   stopping_threshold=0.99))
    
    trainer = pl.Trainer(max_epochs=args.max_epochs, 
                         gradient_clip_val = args.gradient_clipping_val,
                         gradient_clip_algorithm=args.gradient_clipping_algo,
                          precision=args.precision, 
                          max_time=args.max_time,
                          accelerator=args.device,
                          max_steps=args.max_steps,
                          check_val_every_n_epoch=1,
                          logger=logger,
                          log_every_n_steps=1,strategy='ddp',
                          detect_anomaly=True,num_nodes=1,
                          callbacks=callbacks,devices=1) 
    
    
    #-- Load pretrained weights
    if os.path.exists(args.pretraining_weights):
        print(f'\nLoading weigths : {args.pretraining_weights}\n')
        model = model.load_from_checkpoint(checkpoint_path=args.pretraining_weights,args=args)
        trainer.fit(model,
                    train_dataloaders=train_loader,
                    val_dataloaders=val_loader)
    
    else:
        trainer.fit(model,train_dataloaders=train_loader,val_dataloaders=val_loader)
    
    #-- Test
    test_data = AlpineImages(transforms = get_transforms(mode='test'),
                              split = 'test', 
                              data_path=args.data_location)
    
    test_loader = DataLoader(test_data,
                            batch_size=args.batch_size,
                            num_workers=args.num_workers,
                            pin_memory=args.pin_memory,
                            shuffle=False)
    
    trainer.test(model,
                 dataloaders=test_loader,
                 verbose=False,
                 ckpt_path='best')


#-- testing function
def test(args):
    """Evaluate model on test data provided path to the model weights. Read runner_test.py to understand how to use this function.
    

    Parameters
    ----------
    args : dataclass
        Configuration of the model.

    Raises
    ------
    NotImplementedError
        It raised this error if not path to the model weights is given.

    Returns
    -------
    None.

    """
    test_data = AlpineImages(transforms = get_transforms(mode='test'),
                              split = 'test', 
                              data_path=args.data_location)
    
    test_loader = DataLoader(test_data,
                            batch_size=args.batch_size,
                            num_workers=args.num_workers,
                            pin_memory=args.pin_memory,
                            shuffle=False)
    
    img,target = next(iter(test_loader))    
    print('\nShape of input & target : ', img.shape,target.shape,'\n')
    print('Size of test : ',len(test_data),'\n')

    
    #-- Create model
    model = Architecture(args)
       
    #-- Create trainer
    logger = CSVLogger("csv-logs", name="my_exp")
    if args.logger == 'wandb':
        logger = WandbLogger(project=args.project_name,log_model=args.save_weights_only)
        

    #-- Lr monitor
    lr_monitor = LearningRateMonitor(logging_interval='epoch')
    checkpointer = pl.callbacks.ModelCheckpoint(monitor=args.metric_to_monitor_early_stop,mode='max',auto_insert_metric_name=True,
                                                save_on_train_epoch_end=False,save_weights_only=args.save_weights_only)
    callbacks = [lr_monitor,checkpointer]

    #-- Checking for early stopping
    if args.early_stopping:
        callbacks.append(EarlyStopping(monitor=args.metric_to_monitor_early_stop,
                   mode=args.early_stop_mode,check_on_train_epoch_end=False,
                   patience=args.early_stopping_patience,
                   verbose=False,
                   min_delta=1e-3,
                   stopping_threshold=0.99))
    
    trainer = pl.Trainer(max_epochs=args.max_epochs, 
                         gradient_clip_val = args.gradient_clipping_val,
                         gradient_clip_algorithm=args.gradient_clipping_algo,
                          precision=args.precision, 
                          max_time=args.max_time,
                          accelerator=args.device,
                          max_steps=args.max_steps,
                          check_val_every_n_epoch=1,
                          logger=logger,
                          log_every_n_steps=1,strategy='ddp',
                          detect_anomaly=True,num_nodes=1,
                          callbacks=callbacks,
                          devices=1)
    
    
    #-- Load pretrained weights
    if os.path.exists(args.pretraining_weights):
        print(f'\nLoading weigths : {args.pretraining_weights}\n')
        trainer.test(model,
                    dataloaders=test_loader,
                    verbose=False,
                    ckpt_path=args.pretraining_weights)
    
    else:
        raise NotImplementedError   










