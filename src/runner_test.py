# -*- coding: utf-8 -*-
"""
Created on Tue Jan 10 23:46:10 2023

@author: fadel
"""

from args import Args
import pytorch_lightning as pl
from datargs import parse
import wandb
import os
from models import test

args = parse(Args)
pl.seed_everything(args.seed,workers=True) #-- for reproducibility


weights_paths ={
    'pspnet/dice+ce':"./weigths/18wdrp6p/checkpoints/epoch=15-step=1840.ckpt"
    }

for name,path in weights_paths.items():
    
    args.run_name = name
    args.pretraining_weights = path
    
    if __name__ == '__main__':
        
        if args.logger == 'wandb':
            #-- Initialize logger
            wandb.init(config=args,
                      project=args.project_name,
                      name=args.run_name,
                      entity='ipeo-epfl',
                      tags= args.tag)
                    
            test(args)
            
            wandb.finish() #-- End
            
        else:
            print('logging in CSV file...')
            test(args)
            
            