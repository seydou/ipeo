from args import Args
import pytorch_lightning as pl
from datargs import parse
import wandb
import os
from models import train,test

args = parse(Args)
pl.seed_everything(args.seed,workers=True) #-- for reproducibility

   

if __name__ == '__main__':
    
    if args.logger == 'wandb':
        #-- Initialize logger
        wandb.init(config=args,
                  project=args.project_name,
                  name=args.run_name,
                  entity='ipeo-epfl',
                  tags= args.tag)
        
        # wandb.run.log_code(".")
        
        train(args)
                
        wandb.finish() #-- End
        
    else:
        print('logging in CSV file...')
        train(args)
    















