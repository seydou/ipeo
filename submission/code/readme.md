# IPEO 2022 Project Submission Template

## Evaluation Repository

Please submit a link to your code repository in your final project report.
Provide (modify) this `readme.md` to tell us how to install/setup the python environment and start

we will run `evaluation.ipynb` and grade based on whether your code runs on our machines and if it reproduces the same (or similar plots) as in your report.

## Data
The data is avaialable at : 
https://drive.google.com/drive/folders/1j5s2u8YRNoiVMQQ1vqKVBUilaL-3AqlS?usp=share_link
or https://enacshare.epfl.ch/fQHAmeKDY6vnMxiEFRzyaP7csVXfNtg
Please download the data, then extract the zip and insert 'ipeo_data' inside './data'

## Access repository
You can access our reporsitory at https://gitlab.epfl.ch/seydou/ipeo.git

## Setup

please tell us how to install dependencies in a new python environment for this code:
```
# create a local virtual environment in the venv folder
python -m venv venv
# activate this environment
source venv/bin/activate
# install requirements
pip install -r requirements.txt
```

we will start jupyter notebook in this environment and then run your evaluation.ipynb
```
jupyter notebook
```


## Repostory Size and Runtime Considerations

* we will not train a model for you. Provide a checkpoint and some (few) test data to reproduce your results.
* please do not commit massively large files (>500MB) in a repository. Maybe add one final model checkpoint that you do not update anymore (any update of large files in this repository will increase its size massively).
* if you have larger datasets, make sure that you make it available online (e.g. via GoogleDrive and EnacShare) and tell us how to download it if necessary.
