# IPEO 2022 Project


## Setup

please tell us how to install dependencies in a new python environment for this code:
```
# create a local virtual environment in the venv folder
python -m venv venv
# activate this environment
source venv/bin/activate
# install requirements
pip install -r requirements.txt
```

## Data
The data is avaialable at : 
https://drive.google.com/drive/folders/1j5s2u8YRNoiVMQQ1vqKVBUilaL-3AqlS?usp=share_link 
or https://enacshare.epfl.ch/fQHAmeKDY6vnMxiEFRzyaP7csVXfNtg
Please download the data, then extract the zip and insert 'ipeo_data' inside './src/data'

## Run training
``
cd src
python runner.py --lr .001 --max-epochs 15 --model-name "PSPNet" --logger "csv" --batch-size 32 --criterion "focal" --focal-loss-gamma 1  --run-name "pspnet/focal/1" --early-stopping                   
``
You can read more about the different parameters inside ./src/args.py.

## Run testing
``
python runner_test.py --model-name "PSPNet" --logger "csv" --batch-size 32
``
